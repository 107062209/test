const {ccclass, property} = cc._decorator;

@ccclass
export default class level_select extends cc.Component {    
    start () {
        Global.health = 5;
        Global.score = 0;
    }
    
    loadGameLevel1(){
        cc.director.loadScene("start1");
    }
    loadGameLevel2(){
        cc.director.loadScene("start2");
    }
}
