const {ccclass, property} = cc._decorator;

@ccclass
export default class Menu extends cc.Component {
    @property(cc.Node)
    bg: cc.Node = null;
    
    @property(cc.Node)
    title: cc.Node = null;

    @property(cc.Node)
    login: cc.Node = null;

    @property(cc.Node)
    signup: cc.Node = null;
    
    @property(cc.Node)
    email: cc.Node = null;
    
    @property(cc.Node)
    password: cc.Node = null;

    Login(){
        var txtEmail = this.email.getComponent(cc.EditBox);
        var txtPassword = this.password.getComponent(cc.EditBox);
        firebase.auth().signInWithEmailAndPassword(txtEmail.string, txtPassword.string).then(function(result) {
            cc.director.loadScene("level_select")
            cc.log(txtEmail.string+txtPassword.string);
        }).catch(function(error) {
            txtEmail.string = "";
            txtPassword.string = "";
            alert(error);
            cc.log(txtEmail.string + txtPassword.string);
        });
    }
    Signup(){
        var txtEmail = this.email.getComponent(cc.EditBox);
        var txtPassword = this.password.getComponent(cc.EditBox);
        firebase.auth().createUserWithEmailAndPassword(txtEmail.string, txtPassword.string).then(function(result) {
            txtEmail.string = "";
            txtPassword.string = "";
            alert("success");
            cc.log(txtEmail.string+txtPassword.string);
        }).catch(function(error) {
            txtEmail.string = "";
            txtPassword.string = "";
            alert(error);
            cc.log(txtEmail.string+txtPassword.string);
        });
    }
}
