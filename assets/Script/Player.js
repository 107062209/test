cc.Class({
    extends: cc.Component,

    properties: {
        jumpHeight: 0,
        jumpDuration: 0,
        walkLength: 0,
        walkDuration: 0,
        speed: 0,
        onGround: true,
        isDead:false,
        rebornPos : cc.v2(0,0),
        jumpaudio:{
            type : cc.AudioClip,
            default : null
        },
        deathaudio:{
            type : cc.AudioClip,
            default : null
        },
        flagaudio:{
            type : cc.AudioClip,
            default : null
        },
        successaudio:{
            type : cc.AudioClip,
            default : null
        },
        animator:{
            type : cc.Animation,
            default : null
        },
        animateState: null,
        camera:{
            type : cc.Node,
            default : null
        },
        healthtext:{
            type: cc.Node,
            default: null
        },
        health : 0,
        background:{
            type: cc.Node,
            default: null
        }
    },

    onLoad () {
        // Add keydown event trigger
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        // Add keyup event trigger
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        // enable physical system
        cc.director.getPhysicsManager().enabled = true;
        // Get player animator component
        this.animator = this.getComponent(cc.Animation);
    },

    onDestroy () {
        // Close physical system & collision system manager
        cc.director.getCollisionManager().enabled = false;
        cc.director.getCollisionManager().enabled = false;
        // Close key control event trigger
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    },

    start () {
        // enable collision system
        cc.director.getCollisionManager().enabled = true;
        cc.director.getCollisionManager().enabledDrawBoundingBox = true;
        // Record initial reborn position
        this.rebornPos = this.node.position;
        // Play player default animation
        this.animator.pause();
        this.animateState = this.animator.play();
        //Get player health
        this.health = Global.health;
        if(cc.director.getScene().name == "success"){
            // audio
            cc.audioEngine.play(this.successaudio, false, 1);
        }
    },

    update (dt) {
        // Player Move
        this.move(dt);
        // Camera follow
        this.camerafollow();
        // Animation Update
        this.PlayerAnimation();
        // UI Update
        this.UpdateUI(); 
    },

    onKeyDown: function (event) {
        var macro = cc.macro;
        if(!this.isDead){   // Player only can move when it isn't dead
            switch(event.keyCode) {
                case macro.KEY.a:
                case macro.KEY.left:
                    this.turnLeft();
                    break;
                case macro.KEY.d:
                case macro.KEY.right:
                    this.turnRight();
                    break;
                
                case macro.KEY.space:
                    if(this.onGround == true){
                        this.Jump();
                    }
                    break;
                case macro.KEY.q:
                    this.Reborn();
                    break;
            }
        }
    },

    onKeyUp: function (event) {
        
        var macro = cc.macro;
        switch(event.keyCode) {
            // Reset player speed
            case macro.KEY.a:
            case macro.KEY.left:
            case macro.KEY.d:
            case macro.KEY.right:
                this.speed = 0;
                break;
        }
    },

    turnLeft () {
        var scene = cc.director.getScene();
        
        if(scene.name != "success"){
            this.speed = -200;
            this.node.scaleX = -2;
        }
    },

    turnRight () {
        var scene = cc.director.getScene();
        
        if(scene.name != "success"){
            this.speed = 200;
            this.node.scaleX = 2;
        }
    },
   
    Reborn: function(){
        // Return to reborn position
        this.node.x = this.rebornPos.x;
        this.node.y = this.rebornPos.y;
        // Init animation
        this.animator.pause();
        this.animateState = this.animator.play('PlayerIdle');
    },

    Jump: function () {

        var scene = cc.director.getScene();
        
        if(scene.name != "success"){
            this.onGround = false;
            // audio
            cc.audioEngine.play(this.jumpaudio, false, 1);
            // animator
            this.animator.pause();
            this.animateState = this.animator.play('PlayerJump');
            // action
            var jumpAction = cc.jumpBy(this.jumpDuration, cc.v2(0, 0), this.jumpHeight, 1);
            var finishAction = cc.callFunc(() => {
            this.onGround = true;
            this.animator.pause();
            this.animateState = this.animator.play('PlayerIdle');
        });
            this.node.runAction(cc.sequence(jumpAction,finishAction));
        }
    },

    move(dt){
        if(!this.isDead)
            this.node.x += this.speed * dt;
    },

    camerafollow:function(){
        var scene = cc.director.getScene();
        
        if(scene.name == "level1"){
            if(this.node.x < -580)
            {
                this.camera.x = -580;
                //this.background.x = 478;
            }
            else if(this.node.x > 1580)
            {
                this.camera.x = 1580;
            }
            else
            {
                this.camera.x = this.node.x;
                //this.background.x = (this.node.x + 490) * 500 / 2410 + 478;
            }
        }
        else if(scene.name == "level2"){
            if(this.node.x < -580)
                this.camera.x = -580;
            else if(this.node.x > 645)
                this.camera.x = 645;
            else
            {
                this.camera.x = this.node.x;
            }
        }
    },

    PlayerAnimation: function(){
        if(this.isDead){
            this.animator.pause();
            this.animateState = this.animator.play('PlayerDead');   
        }else{
            if(this.speed != 0){
                if(this.animateState == null || this.animateState.name == 'PlayerIdle'){
                    this.animator.pause();
                    this.animateState = this.animator.play('PlayerRun');
                }
            }else if(this.onGround != false && this.speed == 0){
                this.animator.pause();
                this.animateState = this.animator.play('PlayerIdle');
            }
        }
    },

    UpdateUI: function(){
        var scene = cc.director.getScene();
        
        if(scene.name != "success"){
            // Renew health
            this.healthtext.getComponent(cc.Label).string= "health : " + this.health;
        }
    },

    onCollisionEnter: function(other, self){
        console.log('on collision enter');
        console.log(other.tag);
        console.log(self.name);
    },

    onBeginContact: function (contact, selfCollider, otherCollider) {
        // console.log(otherCollider.node.name);
        // "otherCollider.node" can get collider's node 

        // Contact to box
        if(otherCollider.tag == 2){
            console.log("Touch check point");
            if(otherCollider.node.position.x > this.rebornPos.x){
                // audio
                cc.audioEngine.play(this.flagaudio, false, 1);
                this.rebornPos = otherCollider.node.position;
            }
        }
        // Contact to enemy
        if(otherCollider.tag == 3){
            cc.log(contact.getWorldManifold().normal+"player");
            if(contact.getWorldManifold().normal.y>-0.9){
                if(this.health > 1 && this.isDead == false){    // Reborn
                    cc.audioEngine.play(this.deathaudio, false, 1);     // audio
                    this.health -= 1;
                    this.isDead = true;
                    this.schedule(function() {  
                        this.Reborn();
                        this.isDead = false;
                    }, 1, 0);
                }else if(this.health == 1 && this.isDead == false){ // Game over
                    cc.audioEngine.play(this.deathaudio, false, 1);     // audio
                    this.health -= 1;
                    this.isDead = true;
                    this.schedule(function() {  
                        cc.director.loadScene("end");
                    }, 2, 0);
                }
            }
        }
        // Contact to destination
        if(otherCollider.tag == 5){
            var scene = cc.director.getScene();
            Global.health = this.health;
            if(scene.name == "level1"){
                cc.director.loadScene("level2");
            }else if(scene.name == "level2"){
                cc.director.loadScene("success");
            }
        }
    },
});
