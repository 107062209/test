cc.Class({
    extends: cc.Component,

    properties: {
        speed: 0,
        alive: true,
        animator:{
            type : cc.Animation,
            default : null
        },
        animateState: null
    },

    onLoad () {
        // enable physical system
        cc.director.getPhysicsManager().enabled = true;
        // enable collision system
        cc.director.getCollisionManager().enabled = true;
        // Get component
        this.animator = this.getComponent(cc.Animation);
    },

    onDestroy () {
        cc.director.getCollisionManager().enabled = false;
        cc.director.getCollisionManager().enabled = false;
    },

    start () {
        this.rebornPos = this.node.position;
        this.animateState = this.animator.play('eagleIdle');
        this.schedule(function() {  
            this.speed *= -1;
        }, 1);
    },

    update (dt) {
        this.node.y += this.speed * dt;
    },

});
