cc.Class({
    extends: cc.Component,

    properties: {
        speed: 0,
        alive: true,
        animator:{
            type : cc.Animation,
            default : null
        },
        animateState: null
    },

    onLoad () {
        // enable physical system
        cc.director.getPhysicsManager().enabled = true;
        // enable collision system
        cc.director.getCollisionManager().enabled = true;
        // Get component
        this.animator = this.getComponent(cc.Animation);
    },

    deadEffect()
    {
        this.speed = 0;

        this.getComponent(cc.PhysicsBoxCollider).enabled = false;

        let fade = cc.fadeOut(1);

        this.node.runAction(fade);
    },

    onDestroy () {
        cc.director.getCollisionManager().enabled = false;
        cc.director.getCollisionManager().enabled = false;
    },

    start () {
        this.rebornPos = this.node.position;
        this.animateState = this.animator.play('goomba');
    },

    update (dt) {
        this.node.x += this.speed * dt;
    },

    onBeginContact: function (contact, selfCollider, otherCollider) {
        if(otherCollider.tag == 6)
        {
            this.node.scaleX *= -1;
            this.speed *= -1;
        }
        else if(otherCollider.tag == 10){
            cc.log(contact.getWorldManifold().normal+"enemy");
            if(contact.getWorldManifold().normal.y >=0.9){
                this.animator.stop();
                this.deadEffect();
                cc.log("head");
            }
        }
    },

});
