cc.Class({
    extends: cc.Component,

    properties: {
        jumpHeight: 0,
        jumpDuration: 0,
        alive: true,
        speed : 0,
        animator:{
            type : cc.Animation,
            default : null
        },
        animateState: null
    },

    onLoad () {
        // enable physical system
        cc.director.getPhysicsManager().enabled = true;
        // enable collision system
        cc.director.getCollisionManager().enabled = true;
        // Get component
        this.animator = this.getComponent(cc.Animation);
    },

    onDestroy () {
        cc.director.getCollisionManager().enabled = false;
        cc.director.getCollisionManager().enabled = false;
    },

    start () {
        this.rebornPos = this.node.position;
        this.animateState = this.animator.play('frogIdle');
        this.schedule(function() {
            this.Jump();
        }, 4);
    },

    update (dt) {
    },
    
    deadEffect()
    {
        this.speed = 0;

        this.getComponent(cc.PhysicsBoxCollider).enabled = false;

        let fade = cc.fadeOut(1);

        this.node.runAction(fade);
    },

    onBeginContact: function (contact, selfCollider, otherCollider) {
        if(otherCollider.tag == 10){
            cc.log(contact.getWorldManifold().normal);
            if(contact.getWorldManifold().normal.y >=0.9){
                this.animator.stop();
                this.deadEffect();
                cc.log("head");
            }
        }
    },

    Jump: function () {
    
        // animator
        this.animator.pause();
        if(cc.director.getScene().name == "level1")
            this.animateState = this.animator.play('frogJump_slow');
        else
            this.animateState = this.animator.play('frogJump');
        // action
        var jumpAction = cc.jumpBy(this.jumpDuration, cc.v2(this.speed, 0), this.jumpHeight, 1);
        this.speed *= -1;
        var finishAction = cc.callFunc(() => {
            this.animator.pause();
            this.animateState = this.animator.play('frogIdle');
            this.node.scaleX *= -1;
        });
        this.node.runAction(cc.sequence(jumpAction,finishAction));
    },

});
